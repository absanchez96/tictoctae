import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GameComponent } from './components/game/game.component';
import { LoginComponent } from './login/login/login.component';
import { environment } from 'src/environments/environment';
import { PagenfComponent } from './pagenf/pagenf.component';
import { LoginoneComponent } from './loginone/loginone.component';
import { FormsModule } from '@angular/forms';
import { AngularFireModule } from '@angular/fire/compat';
import { UnirseComponent } from './unirse/unirse.component';


@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    LoginComponent,
    PagenfComponent,
    LoginoneComponent,
    UnirseComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
