# Datos tecnicos para el despliegue

Para el despliegue se recomienda tener las version de Angular CLI 13.1.1, compiler-CLI 13.1.0 y typescript 4.5.2.

Se recomienda, primer paso, instalar las dependencias con "npm install" y con esto se puedo desplegar el software con "ng serve".

El despliegue solo se alcanzo a realizar de forma local ya que con la herramienta Heroku, se obtuvieron contratiempos.

Se genero un envio de datos de autentificacion con FireBase, pero no se alcanzo a desplegar el juego en tiempo real en un Hosting,
por lo cual todas pruebas serian locales, lo datos enviados con FireBase se pueden verificar con software como PostMan o insonmia,
Los requerimientos funcionales del software de formal local se pueden apreciar de forma visual.

Es una pena no poder enviar todo el despliegue completo, es poco, pero hecho con humildad... Saludos!!

# Atictactoe

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
